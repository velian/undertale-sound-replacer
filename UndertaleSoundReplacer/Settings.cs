﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UndertaleSoundReplacer
{
    public partial class Settings : Form
    {
        bool m_steamVersion = false;
        bool m_foundExecutable = false;
        string m_undertaleExecutablePath;


        public Settings()
        {
            InitializeComponent();

            string[] tempFileList = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
            foreach (string item in tempFileList)
            {
                if (System.IO.Path.GetFileName(item).Contains("UNDERTALE.exe"))
                {
                    //Found EXE.
                    m_undertaleExecutablePath = item;
                    m_foundExecutable = true;
                    break;
                }
            }

            if (!m_foundExecutable)
            {
                MessageBox.Show("You must put this application next to your Undertale.EXE file!");
            }
        }

        private void continueButton_Click(object sender, EventArgs e)
        {
            MainWindow window;

            if(radioNoSteam.Checked == true)
            {
                //Start no steam version
                m_steamVersion = false;
            }
            else if(radioSteam.Checked == true)
            {
                //Start steam version
                m_steamVersion = true;
            }

            this.Hide();
            window = new MainWindow(m_steamVersion, m_undertaleExecutablePath);
            window.Closed += (s, args) => this.Close();
            window.Show();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            if(!m_foundExecutable)
            {
                this.Close();
            }
        }
    }
}
