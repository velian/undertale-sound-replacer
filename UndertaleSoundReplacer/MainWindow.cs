﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using System.Diagnostics;
using System.IO;

namespace UndertaleSoundReplacer
{
    public partial class MainWindow : Form
    {
        string m_undertaleExecutablePath;
        string m_currentDirectory;
        string m_currentCustomFolder;
        string m_outputDirectory;

        bool m_shouldClose = false;

        public MainWindow(bool _steamVersion, string _undertalePath)
        {
            InitializeComponent();

            this.AllowDrop = true;
            this.DragDrop += new DragEventHandler(FolderDragDrop);

            ColumnHeader header = new ColumnHeader();
            header.Text = "";
            header.Name = "mainColumn";

            ColumnHeader header2 = new ColumnHeader();
            header2.Text = "";
            header2.Name = "mainColumn";

            customSongsList.Columns.Add(header);
            originalSongList.Columns.Add(header2);

            customSongsList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            originalSongList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            m_undertaleExecutablePath = _undertalePath;
            m_currentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if(_steamVersion)
            {
                steamMenuItem.Checked = true;                
            }
            else
            {
                nonSteamMenuItem.Checked = true;
            }

            FillOriginalSongList();
        }

        private void FillOriginalSongList()
        {
            if(nonSteamMenuItem.Checked)
            {
                //Do lame NON-DRM version.
                m_currentCustomFolder = m_currentDirectory;
                m_outputDirectory = m_currentDirectory;
                string[] customItems = System.IO.Directory.GetFiles(m_currentDirectory, "*.ogg");

                foreach(string item in customItems)
                {
                    originalSongList.Items.Add(System.IO.Path.GetFileName(item));

                }

                if(originalSongList.Items.Count == 0)
                {
                    MessageBox.Show("We couldn't find any sounds next to your Undertale.EXE.\nPerhaps you're running the steam version; try restarting this application with 'STEAM VERSION' selected.");
                    m_shouldClose = true;
                }
            }
            else if(steamMenuItem.Checked)
            {
                //Do non-lame STEAM version here.
                m_outputDirectory = m_currentDirectory + "\\Undertale Sound Modification";

                if (!Directory.Exists(m_currentDirectory + "\\Undertale Sound Modification"))
                {
                  //Make dir and extract .EXE contents
                  Directory.CreateDirectory(m_outputDirectory);
                  ProcessStartInfo p = new ProcessStartInfo();
                  p.FileName = "7za.exe";
                  p.Arguments = "e \"" + m_undertaleExecutablePath + "\" -o\"" + m_outputDirectory;
                  p.WindowStyle = ProcessWindowStyle.Hidden;
                  Process x = Process.Start(p);
                  x.WaitForExit();
                }

                //Read directory
                string[] fileList = System.IO.Directory.GetFiles(m_outputDirectory, "*.ogg");
                string[] fileListDll = System.IO.Directory.GetFiles(m_outputDirectory, "*.dll");

                foreach(string item in fileList)
                {
                    originalSongList.Items.Add(Path.GetFileName(item));
                }    
                
                foreach(string item in fileListDll)
                {
                    if (Path.GetFileName(item) == "steam_api.dll")
                    {
                        System.IO.File.Delete(item);
                    }
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void steamMenuItem_Click(object sender, EventArgs e)
        {
            if(steamMenuItem.Checked) return;
            else
            {
                steamMenuItem.Checked = true;
                nonSteamMenuItem.Checked = false;
            }
        }

        private void nonSteamMenuItem_Click(object sender, EventArgs e)
        {
            if (nonSteamMenuItem.Checked) return;
            else
            {
                nonSteamMenuItem.Checked = true;
                steamMenuItem.Checked = false;
            }
        }

        private void openFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogue = new FolderBrowserDialog();
            dialogue.SelectedPath = m_currentDirectory;
            DialogResult result = dialogue.ShowDialog();

            switch(result)
            {
                case DialogResult.Cancel:

                    break;
                case DialogResult.OK:

                    DisableAll_Click(null, null);
                    customSongsList.Items.Clear();
                    m_currentCustomFolder = dialogue.SelectedPath;

                    string[] customItems = System.IO.Directory.GetFiles(dialogue.SelectedPath, "*.ogg");

                    foreach (string item in customItems)
                    {
                        bool foundRelative = false;

                        foreach (ListViewItem originalItem in originalSongList.Items)
                        {
                            if (originalItem.Text == Path.GetFileName(item))
                            {
                                customSongsList.Items.Add(Path.GetFileName(item));
                                foundRelative = true;
                                continue;
                            }
                        }

                        if (!foundRelative)
                        {
                            ListViewItem newItem = new ListViewItem(Path.GetFileName(item));
                            newItem.BackColor = Color.Red;
                            customSongsList.Items.Add(newItem);
                        }
                    }

                    break;
                default:

                    return;
            }
        }

        private void Enable_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem item in customSongsList.SelectedItems)
            {
                if (item.BackColor == Color.LightBlue) continue;

                foreach(ListViewItem baseSong in originalSongList.Items)
                {
                    if (baseSong.BackColor == Color.LightBlue) continue;

                    if(item.Text == baseSong.Text)
                    {
                        baseSong.BackColor = Color.Orange;
                        item.BackColor = Color.LightGreen;
                        continue;
                    }
                }                
            }
        }

        private void Disable_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in customSongsList.SelectedItems)
            {
                if (item.BackColor == Color.LightBlue) continue;

                foreach (ListViewItem baseSong in originalSongList.Items)
                {
                    if (baseSong.BackColor == Color.LightBlue) continue;

                    if (item.Text == baseSong.Text)
                    {
                        baseSong.BackColor = Color.White;
                        item.BackColor = Color.White;
                        continue;
                    }
                }   
            }
        }

        private void EnableAll_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem item in customSongsList.Items)
            {
                if (item.BackColor == Color.LightBlue) continue;

                foreach (ListViewItem baseSong in originalSongList.Items)
                {
                    if (baseSong.BackColor == Color.LightBlue) continue;

                    if (item.Text == baseSong.Text)
                    {
                        baseSong.BackColor = Color.Orange;
                        item.BackColor = Color.LightGreen;
                    }
                }
            }
        }

        private void DisableAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in customSongsList.Items)
            {
                if (item.BackColor == Color.LightBlue) continue;

                foreach (ListViewItem baseSong in originalSongList.Items)
                {
                    if (baseSong.BackColor == Color.LightBlue) continue;

                    if (item.Text == baseSong.Text)
                    {
                        baseSong.BackColor = Color.White;
                        item.BackColor = Color.White;
                    }
                }
            }
        }

        private void ReplaceListings_Click(object sender, EventArgs e)
        {
            //Replace all the files on the right with the selected ones on the left

            if (m_currentCustomFolder == null) return;

            string[] fileList = System.IO.Directory.GetFiles(m_currentCustomFolder, "*.ogg");

            //foreach (string item in fileList)
            //{
            //    originalSongList.Items.Add(Path.GetFileName(item));
            //}

            bool replacedItem = false;

            foreach (ListViewItem item in customSongsList.Items)
            {
                if(item.BackColor != Color.LightGreen) continue;

                foreach(ListViewItem originItem in originalSongList.Items)
                {
                    if (originItem.BackColor != Color.Orange) continue;

                    foreach (string file in fileList)
                    {
                        if (item.Text == Path.GetFileName(file))
                        {
                            System.IO.File.Copy(file, m_outputDirectory + "\\" + item.Text, true);
                            item.BackColor = Color.LightBlue;
                            originItem.BackColor = Color.LightBlue;
                            replacedItem = true;
                            break;
                        }
                    }

                    if(replacedItem)
                    {
                        replacedItem = false;
                        break;
                    }
                }                
            }

            MessageBox.Show("Replaced all the highlighted listings!");
        }

        private void FolderDragDrop(object sender, DragEventArgs e)
        {
            //Do lame NON-DRM version.
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                m_currentCustomFolder = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];

                string[] customItems = Directory.GetFiles(m_currentCustomFolder, "*.ogg");

                foreach (string item in customItems)
                {
                    bool foundRelative = false;

                    foreach (ListViewItem originalItem in originalSongList.Items)
                    {
                        //if(originalItem.Name != Path.GetFileName(item))
                        //{
                        //    continue;
                        //}
                        if (originalItem.Text == Path.GetFileName(item))
                        {
                            customSongsList.Items.Add(Path.GetFileName(item));
                            foundRelative = true;
                            continue;
                        }

                        //ListViewItem newItem = new ListViewItem(Path.GetFileName(item));

                        

                        //if (Path.GetFileName(item) != originalItem.Text)
                        //{
                            //customSongsList.Items = Color.Red;
                            //break;
                        //}
                    }

                    if(!foundRelative)
                    {
                        ListViewItem newItem = new ListViewItem(Path.GetFileName(item));
                        newItem.BackColor = Color.Red;
                        customSongsList.Items.Add(newItem);
                    }
                }
            }
        }

        private void FolderDragOver(object sender, DragEventArgs e)
        {
            if(!e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            if(m_shouldClose)
            {
                this.Close();
            }
        }

        private void OnItemClick(object sender, EventArgs e)
        {
            foreach (ListViewItem item in customSongsList.SelectedItems)
            {
                if (item.BackColor == Color.LightBlue) continue;

                foreach (ListViewItem baseSong in originalSongList.Items)
                {
                    if (baseSong.BackColor == Color.LightBlue) continue;

                    if (item.Text == baseSong.Text)
                    {
                        if (baseSong.BackColor == Color.Orange)
                            baseSong.BackColor = Color.White;
                        else
                            baseSong.BackColor = Color.Orange;

                        if (item.BackColor == Color.LightGreen)
                            item.BackColor = Color.White;
                        else                       
                            item.BackColor = Color.LightGreen;
                        continue;
                    }
                }
            }
            customSongsList.SelectedItems.Clear();
        }
    }
}
