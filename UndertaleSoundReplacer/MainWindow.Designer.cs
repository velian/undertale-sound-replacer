﻿namespace UndertaleSoundReplacer
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.File = new System.Windows.Forms.ToolStripMenuItem();
            this.openFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.selectVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.steamMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nonSteamMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customSongsList = new System.Windows.Forms.ListView();
            this.originalSongList = new System.Windows.Forms.ListView();
            this.EnableAll = new System.Windows.Forms.Button();
            this.DisableAll = new System.Windows.Forms.Button();
            this.ReplaceListings = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(514, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // File
            // 
            this.File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFolderToolStripMenuItem,
            this.toolStripSeparator1,
            this.selectVersionToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.File.Name = "File";
            this.File.Size = new System.Drawing.Size(37, 20);
            this.File.Text = "File";
            // 
            // openFolderToolStripMenuItem
            // 
            this.openFolderToolStripMenuItem.Name = "openFolderToolStripMenuItem";
            this.openFolderToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.openFolderToolStripMenuItem.Text = "Open Custom Sound Folder...";
            this.openFolderToolStripMenuItem.Click += new System.EventHandler(this.openFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(227, 6);
            // 
            // selectVersionToolStripMenuItem
            // 
            this.selectVersionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.steamMenuItem,
            this.nonSteamMenuItem});
            this.selectVersionToolStripMenuItem.Name = "selectVersionToolStripMenuItem";
            this.selectVersionToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.selectVersionToolStripMenuItem.Text = "Current Version";
            // 
            // steamMenuItem
            // 
            this.steamMenuItem.Enabled = false;
            this.steamMenuItem.Name = "steamMenuItem";
            this.steamMenuItem.Size = new System.Drawing.Size(135, 22);
            this.steamMenuItem.Text = "Steam";
            this.steamMenuItem.Click += new System.EventHandler(this.steamMenuItem_Click);
            // 
            // nonSteamMenuItem
            // 
            this.nonSteamMenuItem.Enabled = false;
            this.nonSteamMenuItem.Name = "nonSteamMenuItem";
            this.nonSteamMenuItem.Size = new System.Drawing.Size(135, 22);
            this.nonSteamMenuItem.Text = "Non-Steam";
            this.nonSteamMenuItem.Click += new System.EventHandler(this.nonSteamMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(227, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.exitToolStripMenuItem.Text = "Exit...";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // customSongsList
            // 
            this.customSongsList.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.customSongsList.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.customSongsList.AllowDrop = true;
            this.customSongsList.AutoArrange = false;
            this.customSongsList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.customSongsList.HideSelection = false;
            this.customSongsList.Location = new System.Drawing.Point(12, 57);
            this.customSongsList.MultiSelect = false;
            this.customSongsList.Name = "customSongsList";
            this.customSongsList.ShowGroups = false;
            this.customSongsList.Size = new System.Drawing.Size(185, 348);
            this.customSongsList.TabIndex = 1;
            this.customSongsList.UseCompatibleStateImageBehavior = false;
            this.customSongsList.View = System.Windows.Forms.View.Details;
            this.customSongsList.SelectedIndexChanged += new System.EventHandler(this.OnItemClick);
            this.customSongsList.DragDrop += new System.Windows.Forms.DragEventHandler(this.FolderDragDrop);
            this.customSongsList.DragOver += new System.Windows.Forms.DragEventHandler(this.FolderDragOver);
            // 
            // originalSongList
            // 
            this.originalSongList.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.originalSongList.AutoArrange = false;
            this.originalSongList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.originalSongList.Location = new System.Drawing.Point(317, 57);
            this.originalSongList.Name = "originalSongList";
            this.originalSongList.ShowGroups = false;
            this.originalSongList.Size = new System.Drawing.Size(185, 348);
            this.originalSongList.TabIndex = 2;
            this.originalSongList.UseCompatibleStateImageBehavior = false;
            this.originalSongList.View = System.Windows.Forms.View.Details;
            // 
            // EnableAll
            // 
            this.EnableAll.Location = new System.Drawing.Point(203, 302);
            this.EnableAll.Name = "EnableAll";
            this.EnableAll.Size = new System.Drawing.Size(108, 23);
            this.EnableAll.TabIndex = 5;
            this.EnableAll.Text = "EnableAll";
            this.EnableAll.UseVisualStyleBackColor = true;
            this.EnableAll.Click += new System.EventHandler(this.EnableAll_Click);
            // 
            // DisableAll
            // 
            this.DisableAll.Location = new System.Drawing.Point(203, 331);
            this.DisableAll.Name = "DisableAll";
            this.DisableAll.Size = new System.Drawing.Size(108, 23);
            this.DisableAll.TabIndex = 6;
            this.DisableAll.Text = "DisableAll";
            this.DisableAll.UseVisualStyleBackColor = true;
            this.DisableAll.Click += new System.EventHandler(this.DisableAll_Click);
            // 
            // ReplaceListings
            // 
            this.ReplaceListings.Location = new System.Drawing.Point(203, 382);
            this.ReplaceListings.Name = "ReplaceListings";
            this.ReplaceListings.Size = new System.Drawing.Size(108, 23);
            this.ReplaceListings.TabIndex = 7;
            this.ReplaceListings.Text = "Replace Listings";
            this.ReplaceListings.UseVisualStyleBackColor = true;
            this.ReplaceListings.Click += new System.EventHandler(this.ReplaceListings_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(410, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Undertale Sounds";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Custom Sounds";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Drag And Drop A Folder Here";
            // 
            // MainWindow
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 417);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ReplaceListings);
            this.Controls.Add(this.DisableAll);
            this.Controls.Add(this.EnableAll);
            this.Controls.Add(this.originalSongList);
            this.Controls.Add(this.customSongsList);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Undertale Sound Replacement Tool";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem File;
        private System.Windows.Forms.ToolStripMenuItem openFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem selectVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem steamMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nonSteamMenuItem;
        private System.Windows.Forms.ListView customSongsList;
        private System.Windows.Forms.ListView originalSongList;
        private System.Windows.Forms.Button EnableAll;
        private System.Windows.Forms.Button DisableAll;
        private System.Windows.Forms.Button ReplaceListings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}