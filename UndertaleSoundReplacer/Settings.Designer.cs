﻿namespace UndertaleSoundReplacer
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.continueButton = new System.Windows.Forms.Button();
            this.radioSteam = new System.Windows.Forms.RadioButton();
            this.radioNoSteam = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // continueButton
            // 
            this.continueButton.Location = new System.Drawing.Point(170, 81);
            this.continueButton.Name = "continueButton";
            this.continueButton.Size = new System.Drawing.Size(90, 29);
            this.continueButton.TabIndex = 0;
            this.continueButton.Text = "Continue";
            this.continueButton.UseVisualStyleBackColor = true;
            this.continueButton.Click += new System.EventHandler(this.continueButton_Click);
            // 
            // radioSteam
            // 
            this.radioSteam.AutoSize = true;
            this.radioSteam.Checked = true;
            this.radioSteam.Location = new System.Drawing.Point(12, 12);
            this.radioSteam.Name = "radioSteam";
            this.radioSteam.Size = new System.Drawing.Size(229, 17);
            this.radioSteam.TabIndex = 1;
            this.radioSteam.TabStop = true;
            this.radioSteam.Text = "I am using the STEAM version of Undertale";
            this.radioSteam.UseVisualStyleBackColor = true;
            // 
            // radioNoSteam
            // 
            this.radioNoSteam.AutoSize = true;
            this.radioNoSteam.Location = new System.Drawing.Point(12, 35);
            this.radioNoSteam.Name = "radioNoSteam";
            this.radioNoSteam.Size = new System.Drawing.Size(248, 17);
            this.radioNoSteam.TabIndex = 2;
            this.radioNoSteam.Text = "I am using the DRM-FREE version of Undertale";
            this.radioNoSteam.UseVisualStyleBackColor = true;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 122);
            this.Controls.Add(this.radioNoSteam);
            this.Controls.Add(this.radioSteam);
            this.Controls.Add(this.continueButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Settings";
            this.Text = "Select Your Version";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button continueButton;
        private System.Windows.Forms.RadioButton radioSteam;
        private System.Windows.Forms.RadioButton radioNoSteam;
    }
}

